/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.util.Arrays;
import java.util.Collections;
import org.fest.assertions.api.Assertions;
import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

/**
 * Tests for the DefaultSemanticsValidator class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class DefaultSemanticsValidatorTest {
    /**
     * This test verifies that the validator detects, when the power profile
     *  does not have a start datetime.
     */
    @Test
    public void validatorDetectsMissingStartTime() {
        final Logger logger = Mockito.mock(Logger.class);
        final DefaultSemanticsValidator validator =
            new DefaultSemanticsValidator(logger);
        final PowerProfile profile = new PowerProfile(
            null,
            DateTime.now(),
            Collections.emptyList()
        );
        Assertions.assertThat(validator.validate(profile)).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error("Start time is missing");
    }

    /**
     * This test verifies that the validator detects, when the power profile
     *  does not have an end datetime.
     */
    @Test
    public void validatorDetectsMissingEndTime() {
        final Logger logger = Mockito.mock(Logger.class);
        final DefaultSemanticsValidator validator =
            new DefaultSemanticsValidator(logger);
        final PowerProfile profile = new PowerProfile(
            DateTime.now(),
            null,
            Collections.emptyList()
        );
        Assertions.assertThat(validator.validate(profile)).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error("End time is missing");
    }

    /**
     * This test verifies that the validator detects, when younicos:end is
     * earlier than, or equal to younicos:start.
     */
    @Test
    public void validatorDetectsStartEndTimeInconsistency() {
        final DateTime now = DateTime.now();
        this.startEndTimeInconsistencyTestLogic(now.plusMillis(1), now);
        this.startEndTimeInconsistencyTestLogic(now, now);
    }

    /**
     * This test verifies that the validator detects the violation
     * of the following constraint:
     * For all timestamp of all entries in younicos:powerSequence following
     * condition holds: 1) younicos:timestamp >= younicos:start and 2)
     * younicos:timestamp <= younicos:end, where >= means "later than or at
     * the same time as" and <= means "earlier than or at the same time as".
     */
    @Test
    public void validatorDetectsTimestampInconsistency() {
        final DateTime pstart = DateTime.now();
        final DateTime pend = pstart.plusMillis(5);
        this.timeStampInconsistencyTestLogic(
            pstart, pend, pstart.minusMillis(1)
        );
        this.timeStampInconsistencyTestLogic(
            pstart, pend, pend.plusMillis(1)
        );
    }

    /**
     * This test verifies that the validator detects, when there is more than
     * one entry in younicos:powerSequence, which covers the same time period
     * and contains different younicos:power_kW values.
     */
    @Test
    public void validatorDetectsOverlappingEntriesWithDifferentPowerValues() {
        final DateTime pstart = DateTime.now();
        final DateTime pend = pstart.plusMillis(20);
        final PowerProfile profile = new PowerProfile(
            pstart,
            pend,
            Arrays.asList(
                new PowerProfileEntry[]{
                    new PowerProfileEntry(pstart.plusMillis(1), 10.),
                    new PowerProfileEntry(pstart.plusMillis(2), 20.),
                    new PowerProfileEntry(pstart.plusMillis(2), 30.),
                }
            )
        );
        final Logger logger = Mockito.mock(Logger.class);
        final DefaultSemanticsValidator validator =
            new DefaultSemanticsValidator(logger);
        Assertions.assertThat(validator.validate(profile)).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "Inconsistent entry timestamp(s)"
        );
    }
    /**
     * This test verifies that validator returns true, if the power profile
     * is valid.
     */
    @Test
    public void validatorDetectsValidPowerProfile() {
        final DateTime pstart = DateTime.now();
        final DateTime pend = pstart.plusMillis(10);
        final PowerProfile profile = new PowerProfile(
            pstart,
            pend,
            Arrays.asList(
                new PowerProfileEntry[]{
                    new PowerProfileEntry(pstart.plusMillis(1), 10.),
                    new PowerProfileEntry(pstart.plusMillis(5), 20.),
                    new PowerProfileEntry(pstart.plusMillis(6), 30.),
                }
            )
        );
        final Logger logger = Mockito.mock(Logger.class);
        final DefaultSemanticsValidator validator =
            new DefaultSemanticsValidator(logger);
        Assertions.assertThat(validator.validate(profile)).isTrue();
        Mockito.verify(logger, Mockito.never()).error(Mockito.anyString());
    }
    /**
     * Encapsulates the test logic for start-end time inconsistency check.
     * @param start Start time to use.
     * @param end End time to use.
     */
    private void startEndTimeInconsistencyTestLogic(final DateTime start,
        final DateTime end) {
        final Logger logger = Mockito.mock(Logger.class);
        final DefaultSemanticsValidator validator =
            new DefaultSemanticsValidator(logger);
        final PowerProfile profile = new PowerProfile(
            start,
            end,
            Collections.emptyList()
        );
        Assertions.assertThat(validator.validate(profile)).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "End time is earlier than or equal to start time"
        );
    }
    /**
     * Encapsulates the test logic of detecting power profile entries with
     *  invalid timestamps.
     * @param pstart Start time of the profile.
     * @param pend End time of the profile.
     * @param timestamp Timestamp of invalid entry.
     */
    private void timeStampInconsistencyTestLogic(
        final DateTime pstart,
        final DateTime pend,
        final DateTime timestamp) {
        final PowerProfileEntry entry = new PowerProfileEntry(timestamp, 0.);
        final PowerProfile profile = new PowerProfile(
            pstart,
            pend,
            Collections.singletonList(entry)
        );
        final Logger logger = Mockito.mock(Logger.class);
        final DefaultSemanticsValidator validator =
            new DefaultSemanticsValidator(logger);
        Assertions.assertThat(validator.validate(profile)).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "Inconsistent power profile entry"
        );
    }
}
