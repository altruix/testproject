/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

/**
 * Tests for the DefaultXsdValidator class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class DefaultXsdValidatorTests {
    /**
     * Validate that xmlValid returns true, when called on an valid XML file.
     */
    @Test
    public void xmlValidReturnsTrueOnValidFiles() {
        this.validXmlTestLogic(
            "src/test/resources/DefaultXsdValidatorTests.valid1.xml"
        );
        this.validXmlTestLogic(
            "src/test/resources/DefaultXsdValidatorTests.valid2.xml"
        );
    }
    /**
     * Validate that xmlValid returns false, when called on an invalid XML file.
     */
    @Test
    public void xmlValidReturnsFalseOnInvalidFiles() {
        this.invalidXmlTestLogic(
            "src/test/resources/DefaultXsdValidatorTests.invalid1.xml"
        );
        this.invalidXmlTestLogic(
            "src/test/resources/DefaultXsdValidatorTests.invalid2.xml"
        );
    }
    /**
     * Encapsultes test logic for valid XML files.
     * @param file File to test.
     */
    private void validXmlTestLogic(final String file) {
        final DefaultXsdValidator validator = new DefaultXsdValidator();
        Assertions.assertThat(
            validator.xmlValid(
                new File(
                    file
                )
            )
        ).isTrue();
    }
    /**
     * Encapsultes test logic for invalid XML files.
     * @param file File to test.
     */
    private void invalidXmlTestLogic(final String file) {
        final Logger logger = Mockito.mock(Logger.class);
        final DefaultXsdValidator validator = new DefaultXsdValidator(logger);
        Assertions.assertThat(
            validator.xmlValid(
                new File(
                    file
                )
            )
        ).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            Mockito.eq(""),
            Mockito.any(Exception.class)
        );
    }
}
