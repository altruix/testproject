/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.fest.assertions.api.Assertions;
import org.joda.time.DateTime;
import org.junit.Test;

/**
 * Tests for PowerProfileEntry.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PowerProfileEntryTests {
    /**
     * Verifies that equals and hashCode methods work for equal objects.
     */
    @Test
    public void equalsAndHashCodeWorkForEqualObjects() {
        final DateTime now = DateTime.now();
        final PowerProfileEntry first = new PowerProfileEntry(now, 6000.);
        final PowerProfileEntry second = new PowerProfileEntry(now, 6000.);
        Assertions.assertThat(first.equals(second)).isTrue();
        Assertions.assertThat(second.equals(first)).isTrue();
        Assertions.assertThat(first.hashCode() == second.hashCode()).isTrue();
    }
    /**
     * Verifies that equals and hashCode methods work for different objects.
     */
    @Test
    public void equalsAndHashCodeWorkForDifferentObjects() {
        final DateTime now = DateTime.now();
        final PowerProfileEntry first = new PowerProfileEntry(
            now.plusDays(1), -6000
        );
        final PowerProfileEntry second = new PowerProfileEntry(
            now, 6000
        );
        Assertions.assertThat(first.equals(second)).isFalse();
        Assertions.assertThat(second.equals(first)).isFalse();
        Assertions.assertThat(first.hashCode() == second.hashCode()).isFalse();
    }
}
