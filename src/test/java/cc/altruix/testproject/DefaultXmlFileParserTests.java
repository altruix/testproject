/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;
import org.fest.assertions.api.Assertions;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

/**
 * Tests for the DefaultXmlFileParser class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class DefaultXmlFileParserTests {
    /**
     * Year 2014.
     */
    private static final int YEAR_2014 = 2014;
    /**
     * One thousand kilowatt-hours.
     */
    private static final double THOUSAND_KWH = 1000.;
    /**
     * February 25th.
     */
    private static final int FEBRUARY_25TH = 25;
    /**
     * February 26th.
     */
    private static final int FEBRUARY_26TH = 26;

    /**
     * Verifies that the read method read all necessary data from the XML file.
     */
    @Test
    public void readReadsAllData() {
        final DefaultXmlFileParser parser = new DefaultXmlFileParser();
        final PowerProfile profile = parser.read(
            new File("src/test/resources/DefaultXmlFileParserTests.xml")
        );
        Assertions.assertThat(profile).isNotNull();
        Assertions.assertThat(profile.getStart()).isEqualTo(
            new DateTime(YEAR_2014, 2, FEBRUARY_25TH, 0, 0, DateTimeZone.UTC)
        );
        Assertions.assertThat(profile.getEnd()).isEqualTo(
            new DateTime(YEAR_2014, 2, FEBRUARY_26TH, 0, 0, DateTimeZone.UTC)
        );
        Assertions.assertThat(profile.getEntries()).isNotNull();
        Assertions.assertThat(profile.getEntries().size()).isEqualTo(2);
        Assertions.assertThat(profile.getEntries()).contains(
            new PowerProfileEntry(
                new DateTime(
                    YEAR_2014,
                    2,
                    FEBRUARY_25TH,
                    0,
                    0,
                    DateTimeZone.UTC
                ),
                THOUSAND_KWH
            )
        );
        Assertions.assertThat(profile.getEntries()).contains(
            new PowerProfileEntry(
                new DateTime(
                    YEAR_2014,
                    2,
                    FEBRUARY_25TH,
                    2,
                    0,
                    DateTimeZone.UTC
                ),
                0
            )
        );
    }
}
