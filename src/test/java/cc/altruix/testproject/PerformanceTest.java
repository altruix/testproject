/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.apache.commons.lang3.time.StopWatch;
import org.fest.assertions.api.Assertions;
import org.junit.Test;

/**
 * This test validates that the application works fast enough.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PerformanceTest {
    /**
     * Number of performance test runs.
     */
    private static final int TEST_RUNS = 100;
    /**
     * Maximum time, which we think is acceptable to take for the method
     *  applicationRunsFastEnough to run (this is an estimate of the
     *  application's performance).
     */
    private static final int MAX_ALLOWED_TIME = 2000;
    /**
     * Verifies that the total time for running the application 500 times takes
     *  less than 2 seconds.
     */
    @Test
    public void applicationRunsFastEnough() {
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for (int ctr = 0; ctr < TEST_RUNS; ++ctr) {
            App.main(
                this.createArray("src/resources/DefaultXmlFileParserTests.xml")
            );
            App.main(
                this.createArray(
                    "src/resources/DefaultXsdValidatorTests.valid1.xml"
                )
            );
            App.main(
                this.createArray(
                    "src/resources/DefaultXsdValidatorTests.invalid1.xml"
                )
            );
            App.main(
                this.createArray(
                    "src/resources/DefaultXsdValidatorTests.valid2.xml"
                )
            );
            App.main(
                this.createArray(
                    "src/resources/DefaultXsdValidatorTests.invalid2.xml"
                )
            );
        }
        stopWatch.stop();
        Assertions.assertThat(stopWatch.getTime()).isLessThanOrEqualTo(
            MAX_ALLOWED_TIME
        );
    }

    /**
     * Creates an array with single element - filename. We need this method in
     *  order to get rid of the PMD's warning AvoidInstantiatingObjectsInLoops.
     * @param filename Filename to be in the command-line arguments array.
     * @return Array with one element - filename.
     */
    private String[] createArray(final String filename) {
        return new String[] {filename, };
    }
}
