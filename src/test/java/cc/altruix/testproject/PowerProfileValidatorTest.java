/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;
import java.util.Collections;
import org.fest.assertions.api.Assertions;
import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

/**
 * Tests for the PowerProfileValidator class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PowerProfileValidatorTest {
    /**
     * XML file path.
     */
    private static final String XML_FILE_PATH = "C:\\someFile.xml";
    /**
     * XML file name.
     */
    private static final String XML_FILE_NAME = "someFile.xml";
    /**
     * Test command line arguments.
     */
    public static final String[] CMD_LINE_ARGS = new String[]{"somefile.xml"};
    /**
     * Verifies that argsValid returns false, if the argument is null.
     */
    @Test
    public void argsValidReturnsFalseOnNull() {
        final Logger logger = Mockito.mock(Logger.class);
        final PowerProfileValidator testobj =
            this.createPowerProfileValidator(logger);
        Assertions.assertThat(testobj.argsValid(null)).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "Missing command-line arguments"
        );
    }

    /**
     * Verifies that argsValid returns false, if there are no arguments in the
     *  array.
     */
    @Test
    public void argsValidReturnsFalseOnEmptyArgs() {
        final Logger logger = Mockito.mock(Logger.class);
        final PowerProfileValidator testobj =
            this.createPowerProfileValidator(logger);
        Assertions.assertThat(testobj.argsValid(new String[0])).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "Wrong number of arguments (0 instead of 1)"
        );
    }
    /**
     * Verifies that argsValid returns false, if there are too many arguments
     *  in the array.
     */
    @Test
    public void argsValidReturnsFalseOnExcessArgs() {
        final Logger logger = Mockito.mock(Logger.class);
        final PowerProfileValidator testobj =
            this.createPowerProfileValidator(logger);
        Assertions.assertThat(testobj.argsValid(new String[2])).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "Wrong number of arguments (2 instead of 1)"
        );
    }
    /**
     * Verifies that argsValid returns false, if provided file is not an XML
     *  file.
     */
    @Test
    public void argsValidReturnsFalseOnNonXmlFile() {
        final Logger logger = Mockito.mock(Logger.class);
        final PowerProfileValidator testobj =
            this.createPowerProfileValidator(logger);
        Assertions.assertThat(
            testobj.argsValid(
                new String[] {"powerprofile.nonxml"}
            )
        ).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "File doesn't have XML extension."
        );
    }
    /**
     * Verifies that argsValid returns false, if provided file can't be read.
     */
    @Test
    public void argsValidReturnsFalseOnUnreadableFile() {
        final Logger logger = Mockito.mock(Logger.class);
        final PowerProfileValidator testobj = Mockito.spy(
            this.createPowerProfileValidator(logger)
        );
        final File unreadableFile = Mockito.mock(File.class);
        Mockito.when(unreadableFile.exists()).thenReturn(true);
        Mockito.when(unreadableFile.canRead()).thenReturn(false);
        Mockito.when(unreadableFile.getAbsolutePath()).thenReturn(
            XML_FILE_PATH
        );
        Mockito.doReturn(unreadableFile).when(testobj).createFile(
            XML_FILE_NAME
        );
        Assertions.assertThat(
            testobj.argsValid(
                new String[]{XML_FILE_NAME}
            )
        ).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "File 'C:\\someFile.xml' is unreadable."
        );
    }

    /**
     * Verifies that argsValid returns false, if provided file does not exist.
     */
    @Test
    public void argsValidReturnsFalseOnNonExistentFile() {
        final Logger logger = Mockito.mock(Logger.class);
        final PowerProfileValidator testobj = Mockito.spy(
            this.createPowerProfileValidator(logger)
        );
        final File nonExistentFile = Mockito.mock(File.class);
        Mockito.when(nonExistentFile.exists()).thenReturn(false);
        Mockito.when(nonExistentFile.getAbsolutePath()).thenReturn(
            XML_FILE_PATH
        );
        Mockito.doReturn(nonExistentFile).when(testobj).createFile(
            XML_FILE_NAME
        );
        Assertions.assertThat(
            testobj.argsValid(
                new String[]{XML_FILE_NAME}
            )
        ).isFalse();
        Mockito.verify(logger, Mockito.times(1)).error(
            "File 'C:\\someFile.xml' doesn't exist."
        );
    }
    /**
     * Verifies that argsValid returns true, if provided command-line arguments
     *  are correct.
     */
    @Test
    public void argsValidReturnsTrueIfEverythingIsOk() {
        final Logger logger = Mockito.mock(Logger.class);
        final PowerProfileValidator testobj = Mockito.spy(
            this.createPowerProfileValidator(logger)
        );
        final File correctFile = Mockito.mock(File.class);
        Mockito.when(correctFile.exists()).thenReturn(true);
        Mockito.when(correctFile.canRead()).thenReturn(true);
        Mockito.doReturn(correctFile).when(testobj).createFile(
            XML_FILE_NAME
        );
        Assertions.assertThat(
            testobj.argsValid(
                new String[]{XML_FILE_NAME}
            )
        ).isTrue();
        Mockito.verify(logger, Mockito.never()).error(Mockito.anyString());
    }
    /**
     * Verifies that PowerProfileValidator.run runs all required checks.
     */
    @Test
    public void runPerformsAllRequiredChecks() {
        final XsdValidator xsdval =
            Mockito.mock(XsdValidator.class);
        Mockito.when(xsdval.xmlValid(Mockito.any(File.class))).thenReturn(true);
        final XmlFileParser parser =
            Mockito.mock(XmlFileParser.class);
        final DateTime now = DateTime.now();
        final PowerProfile profile = new PowerProfile(now,
            now.plusMillis(1),
            Collections.<PowerProfileEntry>emptyList()
        );
        Mockito.when(parser.read(Mockito.any(File.class)))
            .thenReturn(profile);
        final SemanticsValidator semval =
            Mockito.mock(SemanticsValidator.class);
        Mockito.when(semval.validate(profile)).thenReturn(true);
        final PowerProfileValidator validator = Mockito.spy(
            new PowerProfileValidator(
                Mockito.mock(Logger.class), xsdval, parser, semval)
        );
        Mockito.when(validator.argsValid(CMD_LINE_ARGS)).thenReturn(true);
        validator.run(CMD_LINE_ARGS);
        Mockito.verify(xsdval, Mockito.times(1)).xmlValid(
            Mockito.any(File.class)
        );
        Mockito.verify(semval, Mockito.times(1)).validate(profile);
    }
    /**
     * Creates a PowerProfileValidator with specified logger and mocks for all
     *  other parameters.
     * @param logger Logger.
     * @return New PowerProfileValidator instance.
     */
    private PowerProfileValidator createPowerProfileValidator(
        final Logger logger) {
        return new PowerProfileValidator(
            logger,
            Mockito.mock(XsdValidator.class),
            Mockito.mock(XmlFileParser.class),
            Mockito.mock(SemanticsValidator.class)
        );
    }
}
