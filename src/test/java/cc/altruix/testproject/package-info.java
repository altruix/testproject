/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */

/**
 * Unit and integration tests.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
package cc.altruix.testproject;
