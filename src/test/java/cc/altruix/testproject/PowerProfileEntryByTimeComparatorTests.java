/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.fest.assertions.api.Assertions;
import org.joda.time.DateTime;
import org.junit.Test;

/**
 * Tests for PowerProfileEntryByTimeComparator class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PowerProfileEntryByTimeComparatorTests {
    /**
     * This test verifies that compare method returns a negative integer, if
     *  first entry's timestamp is ealier than the one of the second.
     */
    @Test
    public void compareReturnsNegativeNumberIfFirstIsEarlierThanSecond() {
        final PowerProfileEntryByTimeComparator comparator =
            new PowerProfileEntryByTimeComparator();
        final DateTime later = DateTime.now();
        final DateTime earlier = later.minusMillis(1);
        Assertions.assertThat(
            comparator.compare(
                new PowerProfileEntry(earlier, 0.),
                new PowerProfileEntry(later, 0.)
            )
        ).isLessThan(0);
    }
    /**
     * This test verifies that compare method returns a positive integer, if
     *  first entry's timestamp is later than the one of the second.
     */
    @Test
    public void compareReturnsPositiveNumberIfFirstIsLaterThanSecond() {
        final PowerProfileEntryByTimeComparator comparator =
            new PowerProfileEntryByTimeComparator();
        final DateTime later = DateTime.now();
        final DateTime earlier = later.minusMillis(1);
        Assertions.assertThat(
            comparator.compare(
                new PowerProfileEntry(later, 0.),
                new PowerProfileEntry(earlier, 0.)
            )
        ).isGreaterThan(0);
    }
    /**
     * This test verifies that compare method returns zero, if both entries
     *  have equal timestamps.
     */
    @Test
    public void compareReturnsZeroIfTimestampsAreEqual() {
        final PowerProfileEntryByTimeComparator comparator =
            new PowerProfileEntryByTimeComparator();
        final DateTime now = DateTime.now();
        Assertions.assertThat(
            comparator.compare(
                new PowerProfileEntry(now, 0.),
                new PowerProfileEntry(now, 0.)
            )
        ).isEqualTo(0);
    }
}
