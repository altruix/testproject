/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for checking the validity of data inside power
 * profile, such as timestamp ranges.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.LoggerIsNotStaticFinal")
public final class DefaultSemanticsValidator implements SemanticsValidator {
    /**
     * Logger.
     */
    private final transient Logger logger;
    /**
     * Rules, which are used to validate various aspects of the power profile.
     */
    private final transient List<SemanticValidationRule> rules;

    /**
     * Constructor.
     */
    public DefaultSemanticsValidator() {
        this(LoggerFactory.getLogger(DefaultSemanticsValidator.class));
    }

    /**
     * Constructor for testing purposes.
     * @param log Logger.
     */
    DefaultSemanticsValidator(final Logger log) {
        this.logger = log;
        this.rules = Arrays.asList(
            new SemanticValidationRule[]{
                new StartTimeRule(),
                new EndTimeRule(),
                new ProfileStartEndTimeConsistencyRule(),
                new OverlappingInconsistentEntryRule(),
                new InvalidTimestampRuleValidator(),
            }
        );
    }

    /**
     * Validates the semantics of a power profile.
     * @param profile Power profile to validate.
     * @return True, if the data inside the power profile is
     *  correct, false otherwise.
     */
    @Override
    public boolean validate(final PowerProfile profile) {
        return !this.rules.stream()
            .anyMatch(x -> !x.validate(profile, this.logger));
    }
}
