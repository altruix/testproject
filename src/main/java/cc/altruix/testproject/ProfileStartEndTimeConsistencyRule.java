/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.slf4j.Logger;

/**
 * This rule verifies that profile's start time lies before its end time.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class ProfileStartEndTimeConsistencyRule
    implements SemanticValidationRule {
    /**
     * Performa a check on the power profile (start time is before end time).
     * @param profile Power profile to validate.
     * @param logger Logger, to which error messages (if any) should be written.
     * @return True, if profile passed this check, false otherwise.
     */
    @Override
    public boolean validate(final PowerProfile profile, final Logger logger) {
        final boolean valid = profile.getEnd().isAfter(profile.getStart());
        if (!valid) {
            logger.error("End time is earlier than or equal to start time");
        }
        return valid;
    }
}
