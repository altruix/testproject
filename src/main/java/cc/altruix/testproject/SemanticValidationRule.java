/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.slf4j.Logger;

/**
 * Classes implementing this interface represent rules for validating
 *  individual aspects of power profile contents.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface SemanticValidationRule {
    /**
     * Performa a check on the power profile.
     * @param profile Power profile to validate.
     * @param logger Logger, to which error messages (if any) should be written.
     * @return True, if profile passed this check, false otherwise.
     */
    boolean validate(final PowerProfile profile, final Logger logger);
}
