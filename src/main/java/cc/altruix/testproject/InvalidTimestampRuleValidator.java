/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.slf4j.Logger;

/**
 * Verifies that all entries in a power profile have a timestamp earlier then or
 *  equal to profile end time and later then or equal to the start time of the
 *  profile.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class InvalidTimestampRuleValidator
    implements SemanticValidationRule {
    /**
     * Verifies that all entries of power profile have valid timestamps.
     * @param profile Power profile to validate.
     * @param logger Logger, to which error messages (if any) should be written.
     * @return True, if all timestamps of all entries are valid.
     */
    @Override
    public boolean validate(final PowerProfile profile, final Logger logger) {
        final boolean valid = !profile.getEntries().stream().anyMatch(
            x -> x.getTimestamp().isBefore(profile.getStart())
                || x.getTimestamp().isAfter(profile.getEnd())
        );
        if (!valid) {
            logger.error("Inconsistent power profile entry");
        }
        return valid;
    }
}
