/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class for transforming XML file into a PowerProfile object graph.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.LoggerIsNotStaticFinal")
public final class DefaultXmlFileParser implements XmlFileParser {
    /**
     * Datetime format used in XML.
     */
    private static final DateTimeFormatter DATETIME_FORMAT =
        ISODateTimeFormat
            .dateTimeNoMillis()
            .withZone(DateTimeZone.UTC);
    /**
     * Logger.
     */
    private final transient Logger logger;
    /**
     * Constructor.
     */
    public DefaultXmlFileParser() {
        this(LoggerFactory.getLogger(DefaultXmlFileParser.class));
    }
    /**
     * Constructor for testing.
     * @param log Logger.
     */
    DefaultXmlFileParser(final Logger log) {
        this.logger = log;
    }
    /**
     * Parses the specified XML file and returns its data in form of
     *  PowerProfile object graph.
     * @param file File containing the power profile XML.
     * @return Data of the XML file in form of a PowerProfile instance.
     */
    @Override
    public PowerProfile read(final File file) {
        DateTime start = null;
        DateTime end = null;
        final List<PowerProfileEntry> entries = new LinkedList<>();
        try {
            final DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document doc = builder.parse(file);
            final XPathFactory xPathfactory = XPathFactory.newInstance();
            final XPath xpath = xPathfactory.newXPath();
            start = this.readDateTime("powerProfile/start", xpath, doc);
            end = this.readDateTime("powerProfile/end", xpath, doc);
            final XPathExpression expr = xpath.compile(
                "/powerProfile/powerSequence/power"
            );
            final XPathExpression powerxp = xpath.compile(
                "power_kW"
            );
            final XPathExpression timestampxp = xpath.compile("timestamp");
            final NodeList nodelist = (NodeList) expr.evaluate(
                doc, XPathConstants.NODESET
            );
            for (int ctr = 0; ctr < nodelist.getLength(); ++ctr) {
                final Node node = nodelist.item(ctr);
                entries.add(
                    this.createPowerProfileEntry(
                        powerxp,
                        timestampxp,
                        node
                    )
                );
            }
        } catch (
            final XPathExpressionException
                | ParserConfigurationException
                | SAXException
                | IOException exception) {
            this.logger.error("Error while parsing XML file", exception);
        }
        return new PowerProfile(start, end, entries);
    }
    /**
     * Cretes a PowerProfileEntry from the corresponding XML node.
     * @param powerxp XPath expression for extracting power.
     * @param timestampxp XPath expression for extracting timestamp.
     * @param node Node younicos:power.
     * @return PowerProfileEntry with the data of the node.
     * @throws XPathExpressionException Thrown in case of XPath parsing error.
     */
    private PowerProfileEntry createPowerProfileEntry(
        final XPathExpression powerxp,
        final XPathExpression timestampxp,
        final Node node) throws XPathExpressionException {
        final double power = this.extractPower(powerxp, node);
        final DateTime timestamp = this.extractTimestamp(timestampxp, node);
        return new PowerProfileEntry(timestamp, power);
    }
    /**
     * Extracts timestamp from younicos:power node.
     * @param xpath XPath expression for extracting the timestamp.
     * @param node XML node.
     * @return Datetime contained in the younicos:timestamp node.
     * @throws XPathExpressionException Thrown in case of XPath parsing error.
     */
    private DateTime extractTimestamp(final XPathExpression xpath,
        final Node node) throws XPathExpressionException {
        final Node timestampn = (Node) xpath.evaluate(
            node,
            XPathConstants.NODE
        );
        final String timestamptxt = timestampn.getTextContent();
        return DATETIME_FORMAT.parseDateTime(timestamptxt);
    }

    /**
     * Extracts the power value from younicos:power node.
     * @param xpath XPath expression for reading the power value.
     * @param node Node (younicos:power) to get power data from.
     * @return Power in kilowatt-hours, or 0. in case the value can't be parsed.
     * @throws XPathExpressionException Thrown in case of XPath parsing error.
     */
    private double extractPower(final XPathExpression xpath,
        final Node node) throws XPathExpressionException {
        final Node powern = (Node) xpath.evaluate(node, XPathConstants.NODE);
        final String powertxt = powern.getTextContent();
        double power = 0.;
        try {
            power = Double.parseDouble(powertxt);
        } catch (final NumberFormatException exception) {
            this.logger.error(
                String.format(
                    "Can't parse '%s' number.",
                    powertxt
                )
            );
        }
        return power;
    }

    /**
     * Method for reading younicos:start and younicos:end values.
     * @param path Path of the node to read from.
     * @param xpath XPath object.
     * @param doc XML document to read data from.
     * @return Datetime contained in the element identified by path.
     * @throws XPathExpressionException Thrown in case of XPath parsing errors.
     */
    private DateTime readDateTime(
        final String path, final XPath xpath,
        final Document doc) throws XPathExpressionException {
        final XPathExpression expr = xpath.compile(path);
        final Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);
        return DATETIME_FORMAT.parseDateTime(node.getTextContent());
    }
}
