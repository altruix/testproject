/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.util.List;
import org.joda.time.DateTime;

/**
 * Represents the data in a power profile XML.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PowerProfile {
    /**
     * Start datetime of the power profile.
     */
    private final DateTime start;
    /**
     * End datetime of the power profile.
     */
    private final DateTime end;
    /**
     * Entries of the power profile.
     */
    private final List<PowerProfileEntry> entries;
    /**
     * Constructor.
     * @param pstart Start date time of the profile.
     * @param pend End date time of the profile.
     * @param pentries Power profile entries.
     */
    public PowerProfile(final DateTime pstart,
        final DateTime pend, final List<PowerProfileEntry> pentries) {
        this.start = pstart;
        this.end = pend;
        this.entries = pentries;
    }
    /**
     * Returns the start datetime.
     * @return Start datetime of the power profile.
     */
    public DateTime getStart() {
        return this.start;
    }

    /**
     * Returns the end datetime.
     * @return End datetime of the power profile.
     */
    public DateTime getEnd() {
        return this.end;
    }

    /**
     * Returns entries of the power profile.
     * @return Entries of the power profile.
     */
    public List<PowerProfileEntry> getEntries() {
        return this.entries;
    }
}
