/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.slf4j.Logger;

/**
 * Validates that the start time is not null.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class StartTimeRule implements SemanticValidationRule {
    /**
     * Performa a check on the power profile (start time of the profile is not
     *  null).
     * @param profile Power profile to validate.
     * @param logger Logger, to which error messages (if any) should be written.
     * @return True, if profile passed this check, false otherwise.
     */
    @Override
    public boolean validate(final PowerProfile profile, final Logger logger) {
        final boolean valid = profile.getStart() != null;
        if (!valid) {
            logger.error("Start time is missing");
        }
        return valid;
    }
}
