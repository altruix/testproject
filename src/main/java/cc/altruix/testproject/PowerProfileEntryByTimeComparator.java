/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.util.Comparator;

/**
 * Comapares two power profile entries by their timestamps.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PowerProfileEntryByTimeComparator
    implements Comparator<PowerProfileEntry> {
    /**
     * Compares two power profile entires by their timestamps.
     * @param first First entry.
     * @param second Second entry.
     * @return Returns a negative integer, zero, or a positive integer as the
     *  timstamp of o1 is earlier than, equal to, or later than that of o2.
     */
    @Override
    public int compare(final PowerProfileEntry first,
        final PowerProfileEntry second) {
        int result = 0;
        if ((first == null) && (second == null)) {
            result = 0;
        } else if ((first == null) && (second != null)) {
            result = 1;
        } else if ((first != null) && (second == null)) {
            result = -1;
        } else {
            result = first.getTimestamp().compareTo(second.getTimestamp());
        }
        return result;
    }
}
