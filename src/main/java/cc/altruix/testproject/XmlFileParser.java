/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;

/**
 * Classes implementing this interface read data from the XML power profile.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface XmlFileParser {
    /**
     * Parses the specified XML file and returns its data in form of
     *  PowerProfile object graph.
     * @param file File containing the power profile XML.
     * @return Data of the XML file in form of a PowerProfile instance.
     */
    PowerProfile read(final File file);
}
