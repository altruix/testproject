/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */

/**
 * A test project to show that I can program in Java.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
package cc.altruix.testproject;
