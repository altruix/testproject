/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

/**
 * Main entry point of the application.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class App {
    /**
     * Constructor.
     */
    private App() {
    }
    /**
     * Entry point of the application.
     * @param args Command-line arguments.
     */
    public static void main(final String[] args) {
        new PowerProfileValidator().run(args);
    }
}
