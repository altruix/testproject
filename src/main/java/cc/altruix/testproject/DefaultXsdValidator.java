/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * XSD validator. I suppress PMD's
 *  LoggerIsNotStaticFinal warning because I want to be able to test, that
 *  exceptions are logged.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.LoggerIsNotStaticFinal")
public final class DefaultXsdValidator implements XsdValidator {
    /**
     * Logger.
     */
    private final transient Logger logger;
    /**
     * Constructor.
     */
    public DefaultXsdValidator() {
        this(LoggerFactory.getLogger(DefaultXsdValidator.class));
    }
    /**
     * Constructor for testing purposes.
     * @param log Logger.
     */
    DefaultXsdValidator(final Logger log) {
        this.logger = log;
    }
    /**
     * Validates file against Profile.xsd.
     * @param file XML file to validate.
     * @return True, if file conforms to Profile.xsd, false otherwise.
     */
    @Override
    public boolean xmlValid(final File file) {
        boolean valid = false;
        final SchemaFactory schemaFactory = SchemaFactory
            .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        final URL schemaurl = Thread
            .currentThread()
            .getContextClassLoader()
            .getResource("Profile.xsd");
        final StreamSource source = new StreamSource(file);
        try {
            final Schema schema = schemaFactory.newSchema(schemaurl);
            final Validator validator = schema.newValidator();
            validator.validate(source);
            valid = true;
        } catch (final SAXException exception) {
            this.logger.error("", exception);
        } catch (final IOException exception) {
            this.logger.error("", exception);
        }
        return valid;
    }
}
