/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;

/**
 * This rule detects entries in the power profile, which have the same
 * timestamp, but different power values.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class OverlappingInconsistentEntryRule
    implements SemanticValidationRule {
    /**
     * Determines, whether there are entries in the power profile, which have
     * same timestamps, but different power values.
     * @param profile Power profile to validate.
     * @param logger Logger, to which error messages (if any) should be written.
     * @return True, if profile passed this check, false otherwise.
     */
    @Override
    public boolean validate(final PowerProfile profile, final Logger logger) {
        boolean valid = true;
        final List<PowerProfileEntry> entries = profile.getEntries();
        if (entries.size() > 1) {
            Collections.sort(entries, new PowerProfileEntryByTimeComparator());
            PowerProfileEntry preventry = entries.get(0);
            for (int ctr = 1; (ctr < entries.size()) && valid; ++ctr) {
                final PowerProfileEntry curentry = entries.get(ctr);
                if (preventry.getTimestamp().equals(curentry.getTimestamp())
                    && (Double.compare(
                    preventry.getPower(), curentry.getPower()
                ) != 0)) {
                    valid = false;
                }
                preventry = curentry;
            }
        }
        if (!valid) {
            logger.error("Inconsistent entry timestamp(s)");
        }
        return valid;
    }
}
