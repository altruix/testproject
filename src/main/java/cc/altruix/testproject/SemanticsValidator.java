/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

/**
 * Interface of classes for checking the validity of data inside power
 * profile, such as timestamp ranges.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface SemanticsValidator {
    /**
     * Validates the semantics of a power profile.
     * @param profile Power profile to validate.
     * @return True, if the data inside the power profile is
     *  correct, false otherwise.
     */
    boolean validate(final PowerProfile profile);
}
