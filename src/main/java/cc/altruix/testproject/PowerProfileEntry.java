/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

/**
 * Represents a single entry of the power profile.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PowerProfileEntry {
    /**
     * Initial odd number (hash code generation parameter).
     */
    private static final int INITIAL_NUMBER = 17;
    /**
     * Multiplier (hash code generation parameter).
     */
    private static final int MULTIPLIER = 37;
    /**
     * Timestamp.
     */
    private final DateTime timestamp;
    /**
     * Power in kilowatt-hours.
     */
    private final double power;
    /**
     * Constructor.
     * @param ptimestamp Timestamp.
     * @param pppower Power in kilowatt-hours at the specified time.
     */
    public PowerProfileEntry(final DateTime ptimestamp, final double pppower) {
        this.timestamp = ptimestamp;
        this.power = pppower;
    }
    /**
     * Returns the timestamp of the power profile entry.
     * @return Timestamp of the power profile entry.
     */
    public DateTime getTimestamp() {
        return this.timestamp;
    }
    /**
     * Returns the power in kilowatt-hours.
     * @return Power in kilowatt-hours.
     */
    public double getPower() {
        return this.power;
    }

    /**
     * Checks whether another object is equal to this one.
     * @param obj Another object.
     * @return True, if obj is equal to this instence of PowerProfileEntry,
     *  false otherwise.
     */
    @Override
    @SuppressWarnings("PMD.OnlyOneReturn")
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        final PowerProfileEntry rhs = (PowerProfileEntry) obj;
        return new EqualsBuilder()
            .append(this.power, rhs.power)
            .append(this.timestamp, rhs.timestamp)
            .isEquals();
    }

    /**
     * Calculates the hash code of the object.
     * @return Hash code of the object.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(INITIAL_NUMBER, MULTIPLIER)
            .append(this.power)
            .append(this.timestamp)
            .toHashCode();
    }
}
