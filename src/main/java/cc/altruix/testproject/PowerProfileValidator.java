/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains the logic of the application. I suppress PMD's
 *  LoggerIsNotStaticFinal warning because I want to be able to test, what
 *  individual methods write into the logger. For this reason, the logger member
 *  variable must be final, but not static (it's hard to inject mocks into
 *  static member variables).
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.LoggerIsNotStaticFinal")
public class PowerProfileValidator {
    /**
     * Logger.
     */
    private final transient Logger logger;
    /**
     * XSD validator.
     */
    private final transient XsdValidator xsdvalidator;
    /**
     * Object, which reads the XML document and creates a PowerProfile object
     *  graph.
     */
    private final transient XmlFileParser fileparser;
    /**
     * Object for validating data inside the power profile (e. g. timestamp
     *  ranges).
     */
    private final transient SemanticsValidator semvalidator;
    /**
     * Default constructor.
     */
    public PowerProfileValidator() {
        this(LoggerFactory.getLogger(PowerProfileValidator.class),
            new DefaultXsdValidator(),
            new DefaultXmlFileParser(),
            new DefaultSemanticsValidator());
    }
    /**
     * Constructor for testing purposes. Allows you to inject a logger mock and
     *  verify in a unit test that the class writes correct statements into
     *  the logger.
     * @param log Logger.
     * @param xsdval Instance of XSD validator.
     * @param parser XML parser object.
     * @param semval Semantic validator object.
     * @checkstyle ParameterNumberCheck (3 lines)
     */
    PowerProfileValidator(final Logger log, final XsdValidator xsdval,
        final XmlFileParser parser, final SemanticsValidator semval) {
        this.logger = log;
        this.xsdvalidator = xsdval;
        this.fileparser = parser;
        this.semvalidator = semval;
    }
    /**
     * Main entry point of the class. Checks the command-line arguments for
     *  validity, reads the file and validates it.
     * @param args Command-line arguments.
     */
    public final void run(final String[] args) {
        boolean valid = true;
        if (valid && !this.argsValid(args)) {
            valid = false;
        }
        final File file = new File(args[0]);
        if (valid && !this.xsdvalidator.xmlValid(file)) {
            valid = false;
        }
        if (valid) {
            final PowerProfile profile = this.fileparser.read(file);
            this.semvalidator.validate(profile);
        }
    }

    /**
     * Determines, whether the command-line arguments contain a power profile
     *  XML file name.
     * @param args Command-line arguments.
     * @return True, if the command-line arguments are valid, false otherwise.
     */
    protected boolean argsValid(final String[] args) {
        boolean valid = true;
        valid = this.checkArgsForNull(args);
        valid = this.checkArgsLength(valid, args);
        final String filename;
        if (valid) {
            filename = args[0];
        } else {
            filename = "";
        }
        valid = this.checkFileNameForNull(valid, filename);
        valid = this.checkFileNameExtension(valid, filename);
        final File file;
        if (valid) {
            file = this.createFile(filename);
        } else {
            file = null;
        }
        valid = this.checkFileExists(valid, file);
        valid = this.checkFileReadable(valid, file);
        return valid;
    }
    /**
     * Creates a file with filename. We need this method in order to be able to
     *  unit test this class (see calls Mockito.doReturn(...) in
     *  PowerProfileValidatorTest for examples). This method must not be final
     *  because otherwise we can't do those Mockito tricks. Therefore, I
     *  disabled the DesignForExtensionCheck.
     * @param filename Name of the file.
     * @return Instance of java.io.File representing this file.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    protected File createFile(final String filename) {
        return new File(filename);
    }
    /**
     * Checks whether file is readable.
     * @param valid True, if the check should be performed.
     * @param file File to test.
     * @return False, if the file readability check was performed and the file
     *  actually can't be read. Value of valid otherwise.
     */
    protected boolean checkFileReadable(final boolean valid, final File file) {
        boolean result = valid;
        if (valid && !file.canRead()) {
            this.logger.error(
                String.format(
                    "File '%s' is unreadable.",
                    file.getAbsolutePath()
                )
            );
            result = false;
        }
        return result;
    }

    /**
     * Checks, whether the file exists (command-line argument validity check).
     * @param valid True, if the check should be performed.
     * @param file File to test.
     * @return False, if the file existence check has been performed and the
     *  file can't be read. Value of valid otherwise.
     */
    protected boolean checkFileExists(final boolean valid, final File file) {
        boolean result = valid;
        if (valid && !file.exists()) {
            this.logger.error(
                String.format(
                    "File '%s' doesn't exist.",
                    file.getAbsolutePath()
                )
            );
            result = false;
        }
        return result;
    }

    /**
     * Performs the filename extension check (command-line argument validity).
     * @param valid True, if the check should be performed.
     * @param filename File name to test.
     * @return False, if the test was done and the filename does not end with
     *  ".xml". Value of valid otherwise.
     */
    protected boolean checkFileNameExtension(final boolean valid,
        final String filename) {
        boolean result = valid;
        if (valid
            && !filename.toLowerCase(Locale.ENGLISH).endsWith(".xml")) {
            this.logger.error("File doesn't have XML extension.");
            result = false;
        }
        return result;
    }

    /**
     * Checks, whether file name is null.
     * @param valid True, if the check should be performed.
     * @param filename Filename to test.
     * @return False, if the test was performed and the file name is null.
     *  Value of valid otherwise.
     */
    protected boolean checkFileNameForNull(final boolean valid,
        final String filename) {
        boolean result = valid;
        if (valid && (filename == null)) {
            this.logger.error("Empty file name");
            result = false;
        }
        return result;
    }

    /**
     * Checks the correctness of the number of command-line arguments.
     * @param valid True, if the check needs to be done.
     * @param args Command-line arguments to test.
     * @return False, if the test was done and the number of arguments is
     *  different from 1. Value of valid otherwise.
     */
    protected boolean checkArgsLength(final boolean valid, final String[] args) {
        boolean result = valid;
        if (valid && (args.length != 1)) {
            this.logger.error(
                String.format(
                    "Wrong number of arguments (%d instead of 1)",
                    args.length
                )
            );
            result = false;
        }
        return result;
    }

    /**
     * Checks whether the command-line arguments are null.
     * @param args Command-line arguments.
     * @return True, if args is not null, false otherwise.
     */
    protected boolean checkArgsForNull(final String[] args) {
        boolean result = true;
        if (args == null) {
            this.logger.error("Missing command-line arguments");
            result = false;
        }
        return result;
    }
}
