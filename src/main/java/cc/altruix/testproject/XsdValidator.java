/**
 * This file is part of a test code created by Dmitri Pisarenko.
 */
package cc.altruix.testproject;

import java.io.File;

/**
 * Classes, which implement this interface verify that an XML file conforms to
 *  the power profile XSD.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface XsdValidator {
    /**
     * Validates file against Profile.xsd.
     * @param file XML file to validate.
     * @return True, if file conforms to Profile.xsd, false otherwise.
     */
    boolean xmlValid(final File file);
}
